import { render } from '/lib/View';
import Hex from '/lib/Hex';

function renderModel(currentModel) {
  render(currentModel);
  window.requestAnimationFrame(() => {
    renderModel(model);
  });
}

const canvas = document.querySelector('canvas');

const model = {
  center: { x: 0, y: 0 },
  ctx: canvas.getContext('2d'),
  hexes: [],
  scale: 1.0
};

canvas.addEventListener('wheel', ev => {
  ev.preventDefault();
  ev.stopImmediatePropagation();

  const offset = { x: 0, y: 0 };

  if (ev.ctrlKey) {
    // TODO: Figure out zoom center...
    const scale = Math.max(0.5, Math.min(2.0, model.scale - 0.01 * ev.deltaY));
    // const scaleDiff = model.scale - scale;
    // offset.x = -ev.x * scaleDiff;
    // offset.x = -((ev.x - model.center.x) / scale) * scaleDiff;
    model.scale = scale;
  } else {
    // TODO: Figure out maximum proper containment
    offset.x = ev.deltaX;
    offset.y = ev.deltaY;
  }

  model.center = {
    x: Math.min(20.0, model.center.x - offset.x),
    y: Math.min(20.0, model.center.y - offset.y)
  };
});

for (let row = 0; row < 10; row++) {
  for (let col = 0; col < 8; col++) {
    model.hexes.push(new Hex(col, row));
  }
}

renderModel(model);
