import World from '/lib/World';
import { d } from '/lib/Utils';

function zeroPad(num) {
  if (num < 10) return `0${num.toString()}`;
  return num.toString();
}

export default class Hex {
  constructor(column, row) {
    this.label = `${zeroPad(column + 1)}${zeroPad(row + 1)}`;
    this.world = d() > 3 ? new World() : null;
    this.x = 100 + column * 150;
    this.y = column % 2 > 0 ? 186.25 + row * 172.5 : 100 + row * 172.5;
  }
}
