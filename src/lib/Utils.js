export function d(count = 1) {
  let total = 0;
  for (let i = 0; i < count; i++) {
    total += Math.ceil(Math.random() * 6);
  }
  return total;
}

export function d66(count = 1) {
  let total = 0;
  for (let i = 0; i < count; i++) {
    total += d() * 10 + d();
  }
  return total;
}
