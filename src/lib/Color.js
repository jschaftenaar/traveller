const Color = {
  Black: '#3B393C',
  Cyan: '#90CFD4',
  DarkCyan: '#5AC3D9',
  DarkGray: '#69696C',
  DarkGreen: '#73854A',
  DarkOrange: '#D05226',
  DarkPink: '#D25383',
  DarkPurple: '#9183B2',
  DarkRed: '#951F1F',
  DarkTeal: '#036273',
  DarkYellow: '#F9B81A',
  Green: '#B9CB65',
  LightGray: '#ECE9E0',
  Orange: '#F57840',
  Pink: '#E59AB6',
  Purple: '#A9A3CF',
  Red: '#CA424F',
  Teal: '#438889',
  White: '#FCFBF6',
  Yellow: '#FFCD03'
};

export default Color;
