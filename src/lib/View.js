import Color from '/lib/Color';

export function drawHex(ctx, hex) {
  ctx.font = '20px sans-serif';
  ctx.fillStyle = Color.DarkGray;
  ctx.textAlign = 'center';
  ctx.fillText(hex.label, hex.x, hex.y - 64);

  if (hex.world !== null) {
    const width = 12 + 2 * hex.world.size;

    ctx.fillStyle = Color.DarkGray;
    ctx.beginPath();
    ctx.ellipse(
      hex.x,
      hex.y,
      width,
      width,
      (45 * Math.PI) / 180,
      0,
      2 * Math.PI
    );
    ctx.fill();

    ctx.font = '200 18px sans-serif';
    ctx.fillStyle = Color.LightGray;
    ctx.textAlign = 'center';
    ctx.fillText(hex.world.classificationString, hex.x, hex.y + 60);
  }

  drawHexagon(ctx, hex.x, hex.y);
}

export function drawHexagon(ctx, x, y) {
  ctx.beginPath();
  ctx.moveTo(x + 100 * Math.cos(0), y + 100 * Math.sin(0));

  for (let side = 0; side < 7; side++) {
    ctx.lineTo(
      x + 100 * Math.cos((side * 2 * Math.PI) / 6),
      y + 100 * Math.sin((side * 2 * Math.PI) / 6)
    );
  }

  ctx.strokeStyle = Color.LightGray;
  ctx.stroke();
}

export function render(model) {
  const { ctx } = model;

  ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);

  ctx.canvas.setAttribute('height', window.innerHeight);
  ctx.canvas.setAttribute('width', window.innerWidth);

  ctx.scale(model.scale, model.scale);
  ctx.translate(model.center.x, model.center.y);

  model.hexes.forEach(hex => {
    drawHex(ctx, hex);
  });
}
