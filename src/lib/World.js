import { d } from '/lib/Utils';

function contain(num) {
  return Math.max(0, Math.min(15, num));
}

function diameter(size) {
  switch (size) {
    case 0:
      return '< 1000 km';
    case 1:
      return '1600 km';
    case 2:
      return '3200 km';
    case 3:
      return '4800 km';
    case 4:
      return '6400 km';
    case 5:
      return '8000 km';
    case 6:
      return '9600 km';
    case 7:
      return '11200 km';
    case 8:
      return '12800 km';
    case 9:
      return '14400 km';
    case 10:
      return '16000 km';
  }
}

function minimumTechLevel(atmosphere) {
  switch (atmosphere) {
    case 0:
    case 1:
    case 10:
    case 15:
      return 8;

    case 2:
    case 3:
    case 13:
    case 14:
      return 5;

    case 4:
    case 7:
    case 9:
      return 3;

    case 11:
      return 9;

    case 12:
      return 10;

    default:
      return 0;
  }
}

function starport(population) {
  let rating = d(2);
  if (population > 9) rating += 2;
  else if (population > 7) rating += 1;
  else if (population < 3) rating -= 2;
  else if (population < 5) rating -= 1;

  switch (contain(rating)) {
    case 0:
    case 1:
    case 2:
      return 'X';

    case 3:
    case 4:
      return 'E';

    case 5:
    case 6:
      return 'D';

    case 7:
    case 8:
      return 'C';

    case 9:
    case 10:
      return 'B';

    default:
      return 'A';
  }
}

function surfaceGravity(size) {
  switch (size) {
    case 0:
      return 'N/A';
    case 1:
      return '0.05g';
    case 2:
      return '0.15g';
    case 3:
      return '0.25g';
    case 4:
      return '0.35g';
    case 5:
      return '0.45g';
    case 6:
      return '0.7g';
    case 7:
      return '0.9g';
    case 8:
      return '1.0g';
    case 9:
      return '1.25g';
    case 10:
      return '1.4g';
  }
}

function techAtmosphereMod(atmosphere) {
  if (atmosphere < 4 || atmosphere > 9) return 1;
  return 0;
}

function techGovernmentMod(government) {
  switch (government) {
    case 0:
    case 5:
      return 1;

    case 7:
      return 7;

    case 13:
    case 14:
      return -2;

    default:
      return 0;
  }
}

function techHydrographicMod(hydro) {
  if (hydro === 0 || hydro === 9) return 1;
  if (hydro === 10) return 2;
  return 0;
}

function techPopulationMod(population) {
  if ((population > 0 && population < 6) || population === 8) return 1;
  if (population === 9) return 2;
  if (population === 10) return 4;
  return 0;
}

function techSizeMod(size) {
  if (size < 2) return 2;
  if (size < 5) return 1;
  return 0;
}

function techStarportMod(starport) {
  switch (starport) {
    case 'A':
      return 6;
    case 'B':
      return 4;
    case 'C':
      return 2;
    case 'X':
      return -4;
    default:
      return 0;
  }
}

function temperatureMod(atmosphere) {
  switch (atmosphere) {
    case 2:
    case 3:
      return -2;

    case 4:
    case 5:
    case 14:
      return -1;

    case 8:
    case 9:
      return 1;

    case 10:
    case 13:
    case 15:
      return 2;

    case 11:
    case 12:
      return 6;

    default:
      return 0;
  }
}

export default class World {
  constructor() {
    this.size = d(2) - 2;
    this.diameter = diameter(this.size);
    this.surfaceGravity = surfaceGravity(this.size);

    // Cogri 0101 CA6A643-9 N Ri Wa A
    // <Nam> <Col><Row> <StP><Siz><Atm><Hyd><Pop><Gov><Law><TL> <Ba> <Tr> <TZ>

    this.atmosphere = contain(d(2) - 7 + this.size);

    let hydro = d(2) - 7 + this.atmosphere;
    if (this.size < 2) hydro = 0;
    if (this.atmosphere < 2 || this.atmosphere > 9) hydro -= 4;
    this.hydrographic = contain(hydro);
    // temperature! p218

    this.temperature = contain(d(2) + temperatureMod(this.atmosphere));

    this.population = contain(d(2) - 2);
    this.government =
      this.population > 0 ? contain(d(2) - 7 + this.population) : 0;

    // TODO: Determine factions (page 220)

    this.lawLevel = contain(d(2) - 7 + this.government);
    this.starport = starport(this.population);

    let tl =
      d() +
      techAtmosphereMod(this.atmosphere) +
      techGovernmentMod(this.government) +
      techHydrographicMod(this.hydrographic) +
      techPopulationMod(this.population) +
      techSizeMod(this.size) +
      techStarportMod(this.starport);
    this.techLevel = contain(Math.max(minimumTechLevel(this.atmosphere), tl));
  }

  get classificationString() {
    // <StP><Siz><Atm><Hyd><Pop><Gov><Law><TL>
    return [
      this.starport,
      this.size.toString(16),
      this.atmosphere.toString(16),
      this.hydrographic.toString(16),
      this.population.toString(16),
      this.government.toString(16),
      this.lawLevel.toString(16),
      this.techLevel.toString(16)
    ]
      .join('')
      .toUpperCase();
  }

  get codesString() {
    return '';
  }
}
